function Init() 
	require("lfs")
	require("LuaIRC.init")
	require("CommandHandler")
	require("config")
	require("utils")
	require("MySQL")
	User = irc.new({nick = config["nickList"], username = config["username"], realname = config["realname"]})
	local FirstChannel = true
	
	Prefix = ";"
	Test = {}
	requireDir("plugins")
	--requireDir("extensions")
	Run = true
	print(" ")

	User:hook("OnJoin", "Test", function(user, channel)
		if FirstChannel then 
			User:send("QUIT: %s", config["exit"])
			User:sendChat("NickServ", "ID "..config["NickPassword"]) 
			FirstChannel = false
		end
		
		print("[INPUT]"..user.nick.." joined "..channel) 
	end) 

	User:hook("OnChat", "CommandHandler", function(user, channel, message)
		
		if (user.username == "MinecraftB") then 
			rank, name, msg = parseCNBot(message)
			if msg then
				user.nick = name
				user.username = name
				user.rank = rank
				message = msg
			end
		end
		print("[INPUT]("..tostring(channel)..") "..tostring(user.nick)..": "..message)
		CheckUser(user.username, user.nick)()
		if string.sub(message, 0, #Prefix) == Prefix then
			local Tbl = explodeString(" ", message)
			Command = string.sub(Tbl[1], #Prefix+1, #Tbl[1] ) table.remove(Tbl, 1)
			Access = CheckUser(user.username, user.nick)()
			if Command == "exit" and tonumber(Access) >= 8 then error("Bot remotely closed!") end
			print(string.format("%s: Trying command %s with access %s arguements: ", user.nick, Command, Access, table.concat(Tbl, " ")))
			local Success = cmd.TryCommand(Command, tonumber(Access), channel, user, unpack(Tbl))
			if Success then
				if not type(Success) == "boolean" then
					User:sendNotice(user.nick, "Success: "..tostring(Success))
				else
					User:sendNotice(user.nick, "Error: "..tostring(Success))
				end
			else
				User:sendNotice(user.nick, "No Such Command: "..Command.."!")
			end
		end
	end)
	User:hook("OnNotice", "CommandHandler", function(user, channel, message) if user.nick == nil then user.nick = "Server" end print("[INPUT](Notice) "..tostring(user.nick)..": "..message) end)
	User:connect(config["network"], config["port"])
	for _, ChannelName in pairs(config["channels"]) do User:join(ChannelName) end

end