function requireDir(path, oPath)
	local Dirs = {}
	local Files = {}
	sPath = lfs.currentdir().."\\"..path
		if not oPath then oPath = path end
	for file in lfs.dir(sPath) do
		sfile = sPath.."\\"..file
		local Mode = lfs.attributes(sfile, "mode")
		if Mode == "directory" then --requireDir(dirPath, oPath)
		elseif Mode == "file" then
			local ext = string.match( file, "%.(.+)$" )
			file = oPath.."\\"..file
			string.gsub(file, "\\", ".")
			if ext == "lua" then require(string.sub(file, 0, #file-4)) print("Loaded: "..file) end 
		end
	end
end

function PrintTable( t, indent, done )
    done = done or {}
    indent = indent or 0
    for key, value in pairs(t) do
        print( string.rep("  ", indent) )
        if type(value) == "table" and not done[value] then
            done[value] = true
            print(tostring (key) .. ":")
            PrintTable (value, indent + 2, done)
         else
            print (tostring (key) .. "  =  "..tostring(value))
        end
    end
end

function totable ( str )
    local tbl = {}
    for i = 1, string.len( str ) do
        tbl[i] = string.sub( str, i, i )
    end
    return tbl
end

function explodeString(separator, str, withpattern)
    if (separator == "") then return totable( str ) end
    local ret = {}
    local index,lastPosition = 1,1
    if not withpattern then separator = string.gsub( separator, "[%-%^%$%(%)%%%.%[%]%*%+%-%?]", "%%%1" ) end
    for startPosition,endPosition in string.gmatch( str, "()" .. separator.."()" ) do
        ret[index] = string.sub( str, lastPosition, startPosition-1)
        index = index + 1
        lastPosition = endPosition
    end
    ret[index] = string.sub( str, lastPosition)
    return ret
end

function tableCopy(t, lookup_table)
    if (t == nil) then return nil end
    local copy = {}
    setmetatable(copy, getmetatable(t))
    for i,v in pairs(t) do
        if type(v) ~= "table" then
            copy[i] = v
        else
            lookup_table = lookup_table or {}
            lookup_table[t] = copy
            if lookup_table[v] then
                copy[i] = lookup_table[v]
            else
                copy[i] = tableCopy(v,lookup_table) -- not yet copied. copy it.
            end
        end
    end
    return copy
end

function parseCNBot( str )
	str = string.gsub( str, "\3%d%d", "" )
	str = string.gsub(str,"\15","")
	str = string.gsub(str,"%~","")
    local rankname, message = string.match( str, "^<(.+)> (.+)$" )
	if not rankname then return end
    local rank, name = string.match( rankname, "^%((.+)%) *(.+)$" )
    if rank then
        return rank, name, message
    else
        return nil, rankname, message
    end
end

