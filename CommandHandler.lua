local unpack = unpack
local print = print
local tostring = tostring
local pcall = pcall
local CommandTable = {}

module("cmd")

function cmdTable() return CommandTable end

function AddCommand(Command, Access, func, Help) CommandTable[Command] = {} CommandTable[Command].Func = func CommandTable[Command].Access = Access CommandTable[Command].Help = Help end
function RemoveCommand(Command) CommandTable[Command] = nil end
function TryCommand(Command, Access, Chan, User, ...) 
	if CommandTable[Command] then
		if CommandTable[Command].Access <= Access then
			local Rtn = {pcall(CommandTable[Command].Func, Chan, User, ...)}
			local Return = Rtn[2]
			if Rtn[1] then
				if Return then return Return else return true end
			else
				return (User.nick..": "..Rtn[2])
			end
		end
	else
		return false 
	end
end