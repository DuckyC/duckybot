
function mark_multiples(primes)             
	local multiple=primes["candidate"]
	while multiple <= primes["upto"] do
		multiple = multiple + primes["candidate"]
		-- print(string.format("dia multiple=%d", multiple))
		primes[multiple] = 0
	end
end


function find_next_candidate(primes)             
	primes["candidate"] = primes["candidate"] + 1
	while primes[primes["candidate"]] == 0 do
		primes["candidate"] = primes["candidate"] + 1
	end

end


function print_primes(primes)
	for i=1, primearray["upto"] do
		if primearray[i] == 1 then
			print(i)
		end
	end
end


function print_array(primes)
	local Str = ""
	for i=1, primearray["upto"] do
		if primearray[i] == 1 then Str = Str .. string.format("Num %d: %d", primearray[i], i) .. "    " end
	end
	User:sendChat(primearray["Channel"], Str)
end


function mark_all_as_prime(primes)
	for i=1, primearray["upto"] do primearray[i]=1 end
end


function main(UpTo, Chan)
	primearray={
		Channel=Chan,
		upto=UpTo,
		candidate=2,
		maxcandidate=math.sqrt(UpTo)
	}

	mark_all_as_prime(primes)

	while primearray["maxcandidate"] > primearray["candidate"] do
		mark_multiples(primearray)             
		find_next_candidate(primearray)             
	end

	--print_primes(primearray)
	print_array(primearray)
end

cmd.AddCommand("primes", 8, function(chan, user, UpTo) main(tonumber(UpTo), chan) end)