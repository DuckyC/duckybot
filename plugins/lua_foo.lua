
--local http = require("socket.http")

local irc = irc

local output = false
local channel = nil
local active_nick=nil
local force_error = nil
local timeout = nil
local crlimit = 0
local antispam = 0

local native_print=print
local do_antispam=0

local antispam_user={}
local antispam_global={
helpful={}	
}

--levenshtein
--[[
Function: EditDistance

Finds the edit distance between two strings or tables. Edit distance is the minimum number of
edits needed to transform one string or table into the other.
Parameters:
s - A *string* or *table*.
t - Another *string* or *table* to compare against s.
lim - An *optional number* to limit the function to a maximum edit distance. If specified
and the function detects that the edit distance is going to be larger than limit, limit
is returned immediately.
Returns:
A *number* specifying the minimum edits it takes to transform s into t or vice versa. Will
not return a higher number than lim, if specified.
Example:

:EditDistance( "Tuesday", "Teusday" ) -- One transposition.
:EditDistance( "kitten", "sitting" ) -- Two substitutions and a deletion.

returns...

:1
:3
Notes:
* Complexity is O( (#t+1) * (#s+1) ) when lim isn't specified.
* This function can be used to compare array-like tables as easily as strings.
* The algorithm used is Damerau–Levenshtein distance, which calculates edit distance based
off number of subsitutions, additions, deletions, and transpositions.
* Source code for this function is based off the Wikipedia article for the algorithm
<http://en.wikipedia.org/w/index.php?title=Damerau%E2%80%93Levenshtein_distance&oldid=351641537>.
* This function is case sensitive when comparing strings.
* If this function is being used several times a second, you should be taking advantage of
the lim parameter.
* Using this function to compare against a dictionary of 250,000 words took about 0.6
seconds on my machine for the word "Teusday", around 10 seconds for very poorly
spelled words. Both tests used lim.
Revisions:

v1.00 - Initial.
]]
function EditDistance( s, t, lim )
    local s_len, t_len = #s, #t -- Calculate the sizes of the strings or arrays
    if lim and math.abs( s_len - t_len ) >= lim then -- If sizes differ by lim, we can stop here
        return lim
    end
    
    -- Convert string arguments to arrays of ints (ASCII values)
    if type( s ) == "string" then
        s = { string.byte( s, 1, s_len ) }
    end
    
    if type( t ) == "string" then
        t = { string.byte( t, 1, t_len ) }
    end
    
    local min = math.min -- Localize for performance
    local num_columns = t_len + 1 -- We use this a lot
    
    local d = {} -- (s_len+1) * (t_len+1) is going to be the size of this array
    -- This is technically a 2D array, but we're treating it as 1D. Remember that 2D access in the
    -- form my_2d_array[ i, j ] can be converted to my_1d_array[ i * num_columns + j ], where
    -- num_columns is the number of columns you had in the 2D array assuming row-major order and
    -- that row and column indices start at 0 (we're starting at 0).
    
    for i=0, s_len do
        d[ i * num_columns ] = i -- Initialize cost of deletion
    end
    for j=0, t_len do
        d[ j ] = j -- Initialize cost of insertion
    end
    
    for i=1, s_len do
        local i_pos = i * num_columns
        local best = lim -- Check to make sure something in this row will be below the limit
        for j=1, t_len do
            local add_cost = (s[ i ] ~= t[ j ] and 1 or 0)
            local val = min(
                d[ i_pos - num_columns + j ] + 1, -- Cost of deletion
                d[ i_pos + j - 1 ] + 1, -- Cost of insertion
                d[ i_pos - num_columns + j - 1 ] + add_cost -- Cost of substitution, it might not cost anything if it's the same
            )
            d[ i_pos + j ] = val
            
            -- Is this eligible for tranposition?
            if i > 1 and j > 1 and s[ i ] == t[ j - 1 ] and s[ i - 1 ] == t[ j ] then
                d[ i_pos + j ] = min(
                    val, -- Current cost
                    d[ i_pos - num_columns - num_columns + j - 2 ] + add_cost -- Cost of transposition
                )
            end
            
            if lim and val < best then
                best = val
            end
        end
        
        if lim and best >= lim then
            return lim
        end
    end
    
    return d[ #d ]
end

--end levenshtein


local function antispam_line(channel, nick, line)
	--warning: time is not portable

	local exprint=function(...)
		local s=""
	        for k,v in ipairs({...}) do
	                s = s..tostring(v)
	        end
		User:sendChat(channel,s)
	end


	local max_time=30
	local max_lines=10
	local lev_weight=1.5
	local length_weight=0.01
	local time=os.time()
	if not nick then nick="nobody" end
	if not antispam_user[nick] then
		antispam_user[nick]={}
	end
	local times=antispam_user[nick]
	local total_lines=0
	local total_weight=0
	local to_remove={}
	for k,v in pairs(times) do
		if v.ttime<(time-max_time) then
			table.insert(to_remove,k)
		else
			local distance=EditDistance(v.tline,line,40)
			local add=((1/(1+distance))*lev_weight)
			local lenadd=string.len(v.tline)*length_weight
			if lenadd<1 then lenadd=1 end
			--exprint(string.format("# %d+%f from (%f)'%s' to '%s'",distance,add,lenadd,v.tline,line))
			total_lines=total_lines+lenadd
			total_weight=total_weight+add
		end
	end
	for k,v in pairs(to_remove) do
		times[v]=nil
	end
	table.insert(times,{ttime=time,tline=line})
	
	local lenadd=string.len(line)*length_weight
	total_lines=total_lines+lenadd

	local total_final=total_lines+total_weight
	--exprint(string.format("Totals for %s: Line %d Distance %f for %f",nick,total_lines,total_weight,total_final))
	

	if(total_final>3 and string.lower(tostring(channel))=="#computercraft") then
		if not antispam_global.botchan then antispam_global.botchan=0 end
		if(antispam_global.botchan+300<os.time()) then
			antispam_global.botchan=os.time()
			exprint(nick..": please direct spammy commands to #ccbots")
		end
	end

	if(total_final>max_lines) then
		if not antispam_global.helpful[nick] then antispam_global.helpful[nick]=0 end
		if(antispam_global.helpful[nick]+60<os.time()) then
			antispam_global.helpful[nick]=os.time()
			exprint(nick..": antispam trigger")
		end
		return false
	end

	return true
end

function luaprint(...)
	local s = ""
	for k,v in ipairs({...}) do
		--todo: fix for tables
		--if(type(v)=="string") then
			--setmetatable(v,nil) --disabled higher up
		s = s..tostring(v)
		--end
	end
	--todo exploit this
	while type(s) ~= "string" do
		s=tostring(s)
	end

	if s == "" then print("Nothing to output") return end
	output = true
	if do_antispam then
		antispam = antispam + 1
		if (not antispam_line(channel,active_nick,s)) then
			return
		end
		if antispam > 5 then
			force_error = "Message limit exceeded!"
			return
		end
	end
	User:sendChat(channel, s)
end
---paste

do --b64 namespace closure

-- Lua 5.1+ base64 v3.0 (c) 2009 by Alex Kloss <alexthkloss@web.de>
-- licensed under the terms of the LGPL2

-- character table string
local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

-- encoding
function b64enc(data)
    return ((data:gsub('.', function(x) 
        local r,b='',x:byte()
        for i=8,1,-1 do r=r..(b%2^i-b%2^(i-1)>0 and '1' or '0') end
        return r;
    end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
        if (#x < 6) then return '' end
        local c=0
        for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
        return b:sub(c+1,c+1)
    end)..({ '', '==', '=' })[#data%3+1])
end

-- decoding
function b64dec(data)
    data = string.gsub(data, '[^'..b..'=]', '')
    return (data:gsub('.', function(x)
        if (x == '=') then return '' end
        local r,f='',(b:find(x)-1)
        for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
        return r;
    end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
        if (#x ~= 8) then return '' end
        local c=0
        for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
        return string.char(c)
    end))
end

end --b64 namespace closure

function exec(cmd) fp=assert(io.popen(cmd,r),"popen") repeat line=fp:read("*l") if(line~=nil) then luaprint(line) end until line==nil end

do --admin namespace closure

local last={}
local lastn=1

local admin_otp=nil
local admin_otp_expire=0

function admin_runstring(str)
        local f, err = loadstring(str)
        if f == nil then
                luaprint(err)
	else
		local ok, err = pcall(f)
		if not ok then
			luaprint(err)
		end
        end
end

function admin(argstring)

	local sp
	if type(argstring)=="string" then
		sp=string.find(argstring," ")
	end
	local pass
	local cmd
	if sp then
		pass=string.sub(argstring,1,sp-1)
		cmd=string.sub(argstring,sp+1)
		if pass==admin_otp and admin_otp_expire>os.time() then
			admin_otp=nil
			native_print('Do: "'..cmd..'"')
			admin_runstring(cmd)
		else
			--luaprint('password failure, given "'..pass..'", expected "'..admin_otp..'"')
			luaprint('password failure')
		end
	else
		luaprint("usage")
	end
	


	local fp=io.open("/dev/urandom","r")
	local data=b64enc(fp:read(6))
	fp:close()
	local collide=0
	for k,v in pairs(last) do
		if v==data then
			collide=1
		end
	end
	if collide==0 then
		last[lastn]=data
		lastn=lastn+1
		if lastn>20 then
			lastn=1
		end
		admin_otp=data
		admin_otp_expire=os.time()+60 --not portable to random platforms
		native_print("OTP: \""..admin_otp..'"')
	else
		native_print("OTP: unable to random")
		admin_otp=nil
	end

end


end --admin namespace closure

function printarray(a) local str="" for k,v in pairs(a) do str=str.."["..tostring(k).."]=("..tostring(v)..") " end luaprint(str) end

---paste

local sandboxRun
local force_reset

sandbox={}

local secure_run

local function forceErrorHook(...)
	--native_print("eh: ",...)
	--ar="ar:"
	--for k,v in pairs(debug.getinfo(2)) do ar=ar..' ['..tostring(k)..']='..tostring(v) end
	--native_print(ar)
	if os.clock() > timeout then
		force_error = "Time limit exceeded!"
	end
	if gcinfo() > 50000 then
		force_error = "Memory limit exceeded!"
		force_reset = true
	end
	if force_error ~= nil then
		local f = debug.getinfo(2, "f").func
		if f ~= secure_run then
			error(force_error)
		end
	end
end

local secure_loadstring = function(s,...)
	--if s:byte(1) == 27 then return nil, "yes, oh yes" end
	if type(s)~="string" or (string.byte(s,1) == 27) then
		return nil, "Bytecode loading not allowed"
	end
	
	local untrusted_function, message = loadstring(s,...)
	if not untrusted_function then
		return nil, message
	end
	setfenv(untrusted_function, sandbox)
	return untrusted_function
end

local function do_nothing()

end

local function create_msgh(f)
	--todo this shouldn't work like it does, find out why!
	local function msgh(...)
		native_print(string.format("MH F %d",force_error))
		if force_error ~= nil then
			f(...)
		end
		native_print("MH q")
	end
	return msgh
end

local function createSandbox()
	local oldprint = print
	print = luaprint
	local t = {}
	local _G = _G
	local getfenv = getfenv
	local getmetatable = getmetatable
	local coroutine = coroutine
	
	local indext = {}
	
	setmetatable(t, {__index=indext})
	
	for k,v in ipairs({"assert", "collectgarbage", "error", "ipairs", "next", "pairs", "pcall", "print", "rawequal", "rawset", "rawget", "select", "tonumber", "tostring", "type", "unpack", "_VERSION", "PrintTable", "totable", "explodeString"}) do
		indext[v] = _G[v]
	end
	indext.BOLD = string.char(2)
	indext.COLOR = string.char(3)
	indext.NEXTLINE = string.char(10)
	indext.BREAK = string.char(15)
	
	for _,k in ipairs({"coroutine", "string", "table", "math"}) do
		local replacement = {}
		for k2,v2 in pairs(_G[k]) do
			if k2 ~= "gsub" then replacement[k2] = v2 end
		end
		t[k] = replacement
	end
	
	t.string.find=nil

	t.os = {}
	for k,v in ipairs({"clock", "difftime", "time", "date"}) do
		t.os[v] = os[v]
	end
	
	t._G = t
	
	t.coroutine.create = function(f)
		crlimit = crlimit - 1
		if crlimit < 0 then
			force_error = "Coroutine limit exceeded!"
			error(force_error)
		end
		return coroutine.create(function()
			debug.sethook(forceErrorHook, "l")
			f()
		end)
	end
	
	t.coroutine.wrap = function(f)
		local c = t.coroutine.create(f)
		return function()
			local r = {t.coroutine.resume(c)}
			table.remove(r, 1)
			return unpack(r)
		end
	end
	
	indext.getfenv = function()
		return t
	end
	
	indext.getmetatable = function(t2)
		if type(t2) ~= "table" then
			return nil
		end
		if t == t2 then
			return nil
		end
		return getmetatable(t2)
	end
	
	indext.setmetatable = function(t2, mt)
		if type(t2) ~= "table" then
			return
		end
		if t == t2 then
			return
		end
		setmetatable(t2, mt)
	end

	indext.xpcall=function(f,msgh,...)
		call_handler=create_msgh(msgh)
		xpcall(f,call_handler,...)
	end
	
--[[	t.loadstring = function(s)
		-- TODO sandbox this
		--error(2, "No.")
		--if s:byte(1) == 27 then return nil, "yes, oh yes" end
		if type(s)~="string" or (string.byte(s,1) == 27) then error("yes, oh yes",2) end
		local untrusted_function, message = loadstring(s)
		if not untrusted_function then return nil, message end
		setfenv(untrusted_function, t)
		return untrusted_function
	end]]
	t.loadstring=secure_loadstring
	print = oldprint
	return t
end

sandbox = createSandbox()

--local
function sandboxRun(code, name)
	local f, err = secure_loadstring(code, name)
	if f == nil then
		luaprint(err)
		return
	end
	setfenv(f, sandbox) --todo check
	secure_run(f,name)
end

--local
function secure_run(func,name, ...)
	antispam = 0
	--setfenv(func, sandbox) --todo check
	force_error = nil
	crlimit = 20
	timeout = os.clock() + 10
	do_antispam=1
	output = false
	debug.sethook(forceErrorHook, "l")
	
	local ok, err = pcall(func, ...)
	
	debug.sethook()

	do_antispam=0
	antispam = 0
	
	if force_error ~= nil then
		-- get an error message using the same formatting
		--kevin: todo: verify this line
		ok, err = pcall(loadstring("local a={...} error(a[1])", name), force_error)
		if force_reset then
			force_reset = false
			sandbox = createSandbox()
			
			collectgarbage()
			collectgarbage()
		end
	end
	if not ok then
		luaprint(err)
	end
	if not output then
		luaprint("No output.")
	end
end

cmd.AddCommand("lua", 2, function(chan, from, ...)
	msg = table.concat({...}, " ")
	channel = chan
	active_nick = from.nick
	native_print("running \""..msg:sub(6).."\" for "..from.nick.." in "..chan)
	sandbox.irc_channel = tostring(chan)
	sandbox.irc_nick = tostring(from.nick)
	sandboxRun(msg, from.nick)
end, [[Run lua in a sandbox.]])


cmd.AddCommand("resetlua", 5,function(chan, from) 
		sandbox = createSandbox()
		User:sendChat(chan, "Sandbox reset")
end ,[[Reset the lua sandbox.]])
--[[
cmd.AddCommand("luapb", 2, function(chan, from, msg)
	print("Ran command")
	channel = chan
	active_nick = from
	sandbox.irc_channel = chan
	sandbox.irc_nick = from
	
    local query = string.format("http://pastebin.com/raw.php?i=%s", msg)
	print(query)
	local Result = {http.request(query)}
	PrintTable(Result)

	if error==200 then
		sandboxRun(result, from)
	end 
	
	return "Maaaaaaaaybe"
end, ]]-- --[[Run code from pastebin]])