require("CommandHandler")
cmd.AddCommand("test", 10, function(chan, user)
	return chan+user
end)

cmd.AddCommand("ChangeAccess", 10, function(chan, user, UserChange, AccessTo)
	local Whois = User:whois(UserChange)
	local UserChange = Whois.userinfo[3]
	ChangeAccess(UserChange, AccessTo)
	return ("Changed "..UserChange.."'s Access Level to "..AccessTo)
end)
cmd.AddCommand("time", 3, function(chan, user)
	local TT = os.date("*t", os.time())
	User:sendChat(chan, TT["hour"]..":"..TT["min"]) 
end)
cmd.AddCommand("say", 3, function(chan, user, ...) User:sendChat(chan, table.concat({...}, " ")) end)
cmd.AddCommand("pmsg", 3, function(chan, user, chann, ...) User:sendChat(chann, table.concat({...}, " ")) end)

cmd.AddCommand("prefix", 10, function(chan, user, PF) Prefix = PF return ("Changed Prefix to: "..PF)  end)
cmd.AddCommand("nick", 10, function(chan, user, NewNick)
	User:send("NICK %s", NewNick)
end)
cmd.AddCommand("restart", 8, function()
	User:disconnect("Restarting!")
	_G = GTable 

	print("************-------------------************") 
	print("************|||| RESTARTED ||||************") 
	print("************-------------------************") 
	Start() 
	while Run do User:think() end
end)