
SeenTable = {}
SeenTable.Times = {}
User:hook("OnChat", "LastSeen", function(user, channel, message)
	SeenTable.Times[user.nick] = {}
	SeenTable.Times[user.nick].user = user
	SeenTable.Times[user.nick].channel = channel
	SeenTable.Times[user.nick].message = message
	SeenTable.Times[user.nick].time = os.time()
end)

cmd.AddCommand("LastSeen", 1, function(chan, user, WantUser)
	local Usr = SeenTable.Times[WantUser]

	if Usr then
		local TimeU = os.date("*t", Usr.time)
		local TimeN = os.date("*t", os.time())
		local Diff =  {hour = TimeN["hour"] - TimeU["hour"], min = TimeN["min"] - TimeU["min"], sec = TimeN["sec"] - TimeU["sec"]}
		PrintTable(Diff)
		if(Diff["hour"]>0) then	
			User:sendChat(chan, Usr.user.nick.." was last seen "..Diff["hour"].." hours, "..Diff["min"].." minutes and "..Diff["sec"].." seconds ago")
		elseif(Diff["min"]>0) then
			User:sendChat(chan, Usr.user.nick.." was last seen "..Diff["min"].." minutes and "..Diff["sec"].." seconds ago")
		else
			User:sendChat(chan, Usr.user.nick.." was last seen "..Diff["sec"].." seconds ago")
		end
	else
		User:sendChat(chan, WantUser.." has not been seen.")
	end
end)